# Приложение для водителя такси #

### Авторизация и регистрация пользователя ###

* [Стартовый экран авторизации.jpg](https://bitbucket.org/repo/jz7y8p/images/3067996436-1.jpg)
* [Выбор региона с автоматической подстановкой кода региона.jpg](https://bitbucket.org/repo/jz7y8p/images/2574369098-2.jpg)
* [SMS с проверочным кодом.jpg](https://bitbucket.org/repo/jz7y8p/images/3579697896-2.5.jpg)
* [Экран с разделами необходимыми для прохождения регистрации.jpg](https://bitbucket.org/repo/jz7y8p/images/49987780-3.jpg)
* [Экран с информацией о себе.jpg](https://bitbucket.org/repo/jz7y8p/images/3289425101-4.jpg)
* [Выпадающий список с доступными регионами.jpg](https://bitbucket.org/repo/jz7y8p/images/1755413012-5.jpg)
* [Данные об автомобиле.jpg](https://bitbucket.org/repo/jz7y8p/images/708929184-5.5.jpg)
* [Экран с документами необходимыми для прохождения регистрации, с возможностью сделать фото на камеру или выбрать фото из галереи.jpg](https://bitbucket.org/repo/jz7y8p/images/1255687595-6.jpg)