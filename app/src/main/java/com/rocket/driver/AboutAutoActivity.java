package com.rocket.driver;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.ArrayList;

import javax.crypto.SecretKey;

import cz.msebera.android.httpclient.Header;

public class AboutAutoActivity extends Activity {

    private char[] password = "key-store-password".toCharArray();

    private static String hash;
    private static String vehicleNumber;
    private static String vehicleBrandModel;
    private static String vehicleBrand;
    private static String vehicleModel;
    private static String vehicleColor;
    private static int vehicleBrandID;
    private static int vehicleModelID;
    private static int vehicleColorID;
    private static int vehicleSetFlag;
    private static ArrayList colorList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_auto);

        colorList = new ArrayList<String>();

        Intent intent = getIntent();

        vehicleNumber = intent.getStringExtra("VEHICLE_NUMBER_SAVE");
        vehicleBrandModel = intent.getStringExtra("VEHICLE_BRAND_MODEL_SAVE");
        vehicleColorID = intent.getIntExtra("VEHICLE_COLOR_ID_SAVE", 0);
        vehicleBrand = intent.getStringExtra("VEHICLE_BRAND_SAVE");
        vehicleBrandID = intent.getIntExtra("VEHICLE_BRAND_ID_SAVE", 0);
        vehicleModel = intent.getStringExtra("VEHICLE_MODEL_SAVE");
        vehicleModelID = intent.getIntExtra("VEHICLE_MODEL_ID_SAVE", 0);
        vehicleSetFlag = intent.getIntExtra("VEHICLE_SET_FLAG_SAVE", 0);
        System.out.println("PRINT4: " + vehicleNumber + "      " + vehicleBrandModel + "      " + vehicleColorID +  "    " + vehicleBrandID + "    " +  vehicleModelID);

        try {
            String filesDirectory = getFilesDir().getAbsolutePath();
            String encryptedDataFilePath = filesDirectory + File.separator + "rocketstore.keystore";
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(new FileInputStream(encryptedDataFilePath), password);
            KeyStore.ProtectionParameter protParam =
                    new KeyStore.PasswordProtection(password);
            KeyStore.Entry entry = ks.getEntry("secretKeyAlias", protParam);
            SecretKey keyFound = ((KeyStore.SecretKeyEntry) entry).getSecretKey();
            hash = new String(keyFound.getEncoded());

        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        }

        ResponseServer.getJSONObj("colors", hash, new OnJSONResponseCallback() {
            @Override
            public void onJSONResponse(boolean success, JSONObject response) throws JSONException {
                if (success) {
                    TextView textBrandModel = (TextView) findViewById(R.id.text_vehicle_brand_model);
                    EditText textNumber = (EditText) findViewById(R.id.text_vehicle_number);
                    final Spinner spinnerColours = (Spinner) findViewById(R.id.spinner_vehicle_color);

                    final SparseArray colorSparse = new SparseArray();
                    JSONArray colorsArray = response.getJSONArray("result");

                    for (int i = 0; i < colorsArray.length(); i++) {
                        JSONObject brandObj = colorsArray.getJSONObject(i);
                        colorSparse.put(brandObj.getInt("ID"), brandObj.optString("NAME"));
                        colorList.add(brandObj.optString("NAME"));
                    }

                    spinnerColours
                            .setAdapter(new ArrayAdapter<String>(AboutAutoActivity.this,
                                    android.R.layout.simple_spinner_dropdown_item,
                                    colorList));

                    spinnerColours.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            String selectedItem = spinnerColours.getItemAtPosition(i).toString();

                            int indexValue = colorSparse.indexOfValue(selectedItem);
                            final int keyValue = colorSparse.keyAt(indexValue);
                            vehicleColorID = keyValue;

                            System.out.println(indexValue + "    " + keyValue);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    if (vehicleSetFlag == 2) {
                        vehicleBrandModel = vehicleBrand + " " + vehicleModel;
                        textBrandModel.setText(vehicleBrandModel);
                        textNumber.setText(vehicleNumber);
                        spinnerColours.setSelection(vehicleColorID);

                        findViewById(R.id.loading_panel).setVisibility(View.GONE);
                    } else if (vehicleSetFlag == 1) {
                        textBrandModel.setText(vehicleBrandModel);
                        textNumber.setText(vehicleNumber);
                        spinnerColours.setSelection(vehicleColorID);

                        findViewById(R.id.loading_panel).setVisibility(View.GONE);
                    } else {
                        ResponseServer.postJSONObj("profile", hash, new OnJSONResponseCallback() {
                            @Override
                            public void onJSONResponse(boolean success, JSONObject response) throws JSONException {
                                if (success) {
                                    TextView textBrandModel = (TextView) findViewById(R.id.text_vehicle_brand_model);
                                    EditText textNumber = (EditText) findViewById(R.id.text_vehicle_number);
                                    Spinner spinnerColor = (Spinner) findViewById(R.id.spinner_vehicle_color);

                                    try {
                                        vehicleBrand = response.getJSONObject("result").getJSONObject("CAR").getString("BRAND");
                                        vehicleBrandID = response.getJSONObject("result").getJSONObject("CAR").getInt("BRAND_ID");
                                        vehicleModel = response.getJSONObject("result").getJSONObject("CAR").getString("MODEL");
                                        vehicleModelID = response.getJSONObject("result").getJSONObject("CAR").getInt("MODEL_ID");
                                        vehicleNumber = response.getJSONObject("result").getJSONObject("CAR").getString("NUMBER");
                                        vehicleColorID = response.getJSONObject("result").getJSONObject("CAR").getInt("CAR_COLOR_ID");
                                        vehicleColor = response.getJSONObject("result").getJSONObject("CAR").getString("CAR_COLOR");


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    System.out.println(vehicleBrand + "    " + vehicleModel);

                                    if (vehicleBrand != null && !vehicleBrand.isEmpty() && vehicleModel != null && !vehicleModel.isEmpty()) {
                                        textBrandModel.setText(vehicleBrand + " " + vehicleModel);
                                    }


                                    textNumber.setText(vehicleNumber);

                                    ArrayAdapter myAdap = (ArrayAdapter) spinnerColor.getAdapter();
                                    int spinnerPosition = myAdap.getPosition(vehicleColor);
                                    spinnerColor.setSelection(spinnerPosition);

                                    System.out.println(response.getJSONObject("result").toString());

                                    findViewById(R.id.loading_panel).setVisibility(View.GONE);
                                } else {
                                    AlertDialog.Builder builder =
                                            new AlertDialog.Builder(AboutAutoActivity.this);
                                    builder.setTitle("Ошибка!");
                                    builder.setMessage("Не удается установить связь, попробуйте еще раз");

                                    builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                                    builder.show();
                                }
                            }
                        });
                    }
                } else {
                    AlertDialog.Builder builder =
                            new AlertDialog.Builder(AboutAutoActivity.this);
                    builder.setTitle("Ошибка!");
                    builder.setMessage("Не удается установить связь, попробуйте еще раз");

                    builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });
                    builder.show();
                }
            }
        });
    }

    public void onClickVehicleBrand(View v) {
        ResponseServer.getJSONObj("carBrands", hash, new OnJSONResponseCallback() {
            @Override
            public void onJSONResponse(boolean success, JSONObject response) throws JSONException {
                if (success) {
                    TextView textBrandModel = (TextView) findViewById(R.id.text_vehicle_brand_model);
                    EditText textNumber = (EditText) findViewById(R.id.text_vehicle_number);
                    Spinner spinnerColor = (Spinner) findViewById(R.id.spinner_vehicle_color);

                    vehicleBrandModel = textBrandModel.getText().toString();
                    vehicleNumber = textNumber.getText().toString();
                    vehicleColorID = spinnerColor.getSelectedItemPosition();
                    System.out.println("PRINT1: " + vehicleNumber + "      " + vehicleBrandModel + "      " + vehicleColorID +  "    " + vehicleBrandID + "    " +  vehicleModelID);

                    Intent intent = new Intent(AboutAutoActivity.this, BrandSetActivity.class);
                    intent.putExtra("BRANDS_JSON", response.toString());
                    intent.putExtra("VEHICLE_NUMBER_SAVE", vehicleNumber);
                    intent.putExtra("VEHICLE_BRAND_MODEL_SAVE", vehicleBrandModel);
                    intent.putExtra("VEHICLE_COLOR_ID_SAVE", vehicleColorID);
                    intent.putExtra("VEHICLE_BRAND_SAVE", vehicleBrand);
                    intent.putExtra("VEHICLE_BRAND_ID_SAVE", vehicleBrandID);
                    intent.putExtra("VEHICLE_MODEL_SAVE", vehicleModel);
                    intent.putExtra("VEHICLE_MODEL_ID_SAVE", vehicleModelID);
                    finish();
                    startActivity(intent);

                } else {
                    AlertDialog.Builder builder =
                            new AlertDialog.Builder(AboutAutoActivity.this);
                    builder.setTitle("Ошибка!");
                    builder.setMessage("Не удается установить связь, попробуйте еще раз");

                    builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(AboutAutoActivity.this, AboutAutoActivity.class);
                            dialog.dismiss();
                            finish();
                            startActivity(intent);
                        }
                    });
                    builder.show();
                }
            }
        });
    }

    public void onClickReady(View v) {

        System.out.println("PRINT1: " + vehicleBrandID + "      " + vehicleModelID + "      " + vehicleNumber + "      " + vehicleColorID);
        TextView textBrandModel = (TextView) findViewById(R.id.text_vehicle_brand_model);
        EditText textNumber = (EditText) findViewById(R.id.text_vehicle_number);

        vehicleBrandModel = textBrandModel.getText().toString();
        vehicleNumber = textNumber.getText().toString();

        if (vehicleBrandModel.isEmpty() || vehicleNumber.isEmpty() || vehicleBrandID == 0 || vehicleModelID == 0 || vehicleColorID == 0) {
            AlertDialog.Builder builder =
                    new AlertDialog.Builder(this);
            builder.setTitle("Внимание!");
            builder.setMessage("Все поля обязательны для заполнения");

            builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.show();

        } else {

            AlertDialog.Builder builder =
                    new AlertDialog.Builder(AboutAutoActivity.this);
            builder.setTitle("Внимание!");
            builder.setMessage("Вы уверены, что введенные Вами данные верны?");

            builder.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    AsyncHttpClient client = new AsyncHttpClient();
                    RequestParams params = new RequestParams();
                    params.put("BRAND_ID", vehicleBrandID);
                    params.put("MODEL_ID", vehicleModelID);
                    params.put("NUMBER", vehicleNumber);
                    params.put("CAR_COLOR_ID", vehicleColorID);
                    //System.out.println("PRINT_FINAL: " + vehicleBrandID + "      " + vehicleModelID + "      " + vehicleNumber + "      " + vehicleColorID);

                    client.post("http://api.rockettaxi.org:8888/api/0.1/register?hash=" + hash, params, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            Intent intent = new Intent(AboutAutoActivity.this, VerificationActivity.class);
                            finish();
                            startActivity(intent);
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            AlertDialog.Builder builder =
                                    new AlertDialog.Builder(AboutAutoActivity.this);
                            builder.setTitle("Ошибка!");
                            builder.setMessage("Не удается установить связь, попробуйте еще раз");

                            builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(AboutAutoActivity.this, AboutAutoActivity.class);
                                    dialog.dismiss();
                                    finish();
                                    startActivity(intent);
                                }
                            });
                            builder.show();
                        }
                    });
                }
            });

            builder.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builder.show();
        }
    }

    public void onClickClose(View v) {
        Intent intent = new Intent(AboutAutoActivity.this, VerificationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        finish();
        startActivity(intent);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }
}
