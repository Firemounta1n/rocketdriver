package com.rocket.driver;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

import javax.crypto.SecretKey;

import cz.msebera.android.httpclient.Header;

public class AboutMeActivity extends Activity {

    private char[] password = "key-store-password".toCharArray();

    private static String hash;
    private static JSONObject countryJSON;
    private static String driverFirstName;
    private static String driverLastName;
    private static String driverEmail;
    private static String driverRegion;
    private static int driverRegionId;
    private static Boolean driverSetFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_me);

        Intent intent = getIntent();
        driverRegionId = intent.getIntExtra("SELECTED_COUNTRY_ID", 0);
        driverFirstName = intent.getStringExtra("DRIVER_FIRST_NAME_SAVE");
        driverLastName = intent.getStringExtra("DRIVER_LAST_NAME_SAVE");
        driverEmail = intent.getStringExtra("DRIVER_EMAIL_SAVE");
        driverRegion = intent.getStringExtra("DRIVER_REGION_SAVE");
        driverSetFlag = intent.getBooleanExtra("DRIVER_SET_FLAG_SAVE", false);

        try {
            String filesDirectory = getFilesDir().getAbsolutePath();
            String encryptedDataFilePath = filesDirectory + File.separator + "rocketstore.keystore";
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(new FileInputStream(encryptedDataFilePath), password);
            KeyStore.ProtectionParameter protParam =
                    new KeyStore.PasswordProtection(password);
            KeyStore.Entry entry = ks.getEntry("secretKeyAlias", protParam);
            SecretKey keyFound = ((KeyStore.SecretKeyEntry) entry).getSecretKey();
            hash = new String(keyFound.getEncoded());

        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        }

        if (driverSetFlag) {
            EditText textFirstName = (EditText) findViewById(R.id.text_first_name);
            EditText textLastName = (EditText) findViewById(R.id.text_last_name);
            EditText textEmail = (EditText) findViewById(R.id.text_email);
            TextView textRegion = (TextView) findViewById(R.id.text_region);

            textFirstName.setText(driverFirstName);
            textLastName.setText(driverLastName);
            textEmail.setText(driverEmail);
            textRegion.setText(driverRegion);

            findViewById(R.id.loading_panel).setVisibility(View.GONE);
        } else {
            ResponseServer.postJSONObj("profile", hash, new OnJSONResponseCallback() {
                @Override
                public void onJSONResponse(boolean success, JSONObject response) throws JSONException {
                    if (success) {
                        System.out.println(response);
                        EditText textFirstName = (EditText) findViewById(R.id.text_first_name);
                        EditText textLastName = (EditText) findViewById(R.id.text_last_name);
                        EditText textEmail = (EditText) findViewById(R.id.text_email);
                        TextView textRegion = (TextView) findViewById(R.id.text_region);


                        driverFirstName = response.getJSONObject("result").getString("FIRST_NAME");
                        driverLastName = response.getJSONObject("result").getString("LAST_NAME");
                        driverEmail = response.getJSONObject("result").getString("EMAIL");


                        textFirstName.setText(driverFirstName);
                        textLastName.setText(driverLastName);

                        if (driverEmail != "null") {
                            textEmail.setText(driverEmail);
                        }

                        try {
                            driverRegionId = response.getJSONObject("result").getInt("REGION_ID");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        try {
                            driverRegion = response.getJSONObject("result").getJSONObject("TaximeterRegion").getString("NAME");
                            textRegion.setText(driverRegion);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        try {
                            driverRegion = response.getJSONObject("result").getJSONObject("REGION").getString("NAME");
                            textRegion.setText(driverRegion);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        findViewById(R.id.loading_panel).setVisibility(View.GONE);
                    } else {
                        AlertDialog.Builder builder =
                                new AlertDialog.Builder(AboutMeActivity.this);
                        builder.setTitle("Ошибка!");
                        builder.setMessage("Не удается установить связь, попробуйте еще раз");

                        builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(AboutMeActivity.this, AboutMeActivity.class);
                                dialog.dismiss();
                                finish();
                                startActivity(intent);
                            }
                        });
                        builder.show();
                    }
                }
            });
        }
    }

    public void onClickRegion(View v) {
        ResponseServer.getJSONObj("regions", hash, new OnJSONResponseCallback() {
            @Override
            public void onJSONResponse(boolean success, JSONObject response) throws JSONException {
                if (success) {
                    EditText textFirstName = (EditText) findViewById(R.id.text_first_name);
                    EditText textLastName = (EditText) findViewById(R.id.text_last_name);
                    EditText textEmail = (EditText) findViewById(R.id.text_email);

                    driverFirstName = textFirstName.getText().toString();
                    driverLastName = textLastName.getText().toString();
                    driverEmail = textEmail.getText().toString();

                    Intent intent = new Intent(AboutMeActivity.this, CountrySetActivity.class);
                    intent.putExtra("COUNTRY_JSON", response.toString());
                    intent.putExtra("DRIVER_FIRST_NAME_SAVE", driverFirstName);
                    intent.putExtra("DRIVER_LAST_NAME_SAVE", driverLastName);
                    intent.putExtra("DRIVER_EMAIL_SAVE", driverEmail);
                    intent.putExtra("DRIVER_REGION_SAVE", driverRegion);
                    finish();
                    startActivity(intent);
                } else {
                    AlertDialog.Builder builder =
                            new AlertDialog.Builder(AboutMeActivity.this);
                    builder.setTitle("Ошибка!");
                    builder.setMessage("Не удается установить связь, попробуйте еще раз");

                    builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(AboutMeActivity.this, AboutMeActivity.class);
                            dialog.dismiss();
                            finish();
                            startActivity(intent);
                        }
                    });
                    builder.show();
                }
            }
        });
    }

    public void onClickReady(View v) {
        EditText textFirstName = (EditText) findViewById(R.id.text_first_name);
        EditText textLastName = (EditText) findViewById(R.id.text_last_name);
        EditText textEmail = (EditText) findViewById(R.id.text_email);
        TextView textRegion = (TextView) findViewById(R.id.text_region);

        driverFirstName = textFirstName.getText().toString();
        driverLastName = textLastName.getText().toString();
        driverEmail = textEmail.getText().toString();
        driverRegion = textRegion.getText().toString();

        if (driverFirstName.isEmpty() || driverLastName.isEmpty() || driverEmail.isEmpty() || driverRegion.isEmpty() || driverRegionId == 0) {
            AlertDialog.Builder builder =
                    new AlertDialog.Builder(this);
            builder.setTitle("Внимание!");
            builder.setMessage("Все поля обязательны для заполнения");

            builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.show();

        } else {

            if (!driverEmail.matches("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+")) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(this);
                builder.setTitle("Внимание!");
                builder.setMessage("Введите корректную электронную почту");

                builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();

            } else {

                AlertDialog.Builder builder =
                        new AlertDialog.Builder(AboutMeActivity.this);
                builder.setTitle("Внимание!");
                builder.setMessage("Вы уверены, что введенные Вами данные верны?");

                builder.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //System.out.println("PRINT2: " + driverFirstName + "      " + driverLastName + "      " + driverEmail + "      " + driverRegion + "      " + driverRegionId);
                        AsyncHttpClient client = new AsyncHttpClient();
                        RequestParams params = new RequestParams();
                        params.put("FIRST_NAME", driverFirstName);
                        params.put("LAST_NAME", driverLastName);
                        params.put("EMAIL", driverEmail);
                        params.put("REGION_ID", driverRegionId);

                        client.post("http://api.rockettaxi.org:8888/api/0.1/register?hash=" + hash, params, new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                Intent intent = new Intent(AboutMeActivity.this, VerificationActivity.class);
                                finish();
                                startActivity(intent);
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                AlertDialog.Builder builder =
                                        new AlertDialog.Builder(AboutMeActivity.this);
                                builder.setTitle("Ошибка!");
                                builder.setMessage("Не удается установить связь, попробуйте еще раз");

                                builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(AboutMeActivity.this, AboutMeActivity.class);
                                        dialog.dismiss();
                                        finish();
                                        startActivity(intent);
                                    }
                                });
                                builder.show();

                            }
                        });
                    }
                });

                builder.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builder.show();

            }
        }
    }

    public void onClickClose(View v) {
        Intent intent = new Intent(AboutMeActivity.this, VerificationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        finish();
        startActivity(intent);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }
}
