package com.rocket.driver;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.ArrayList;

import javax.crypto.SecretKey;

public class BrandSetActivity extends Activity {

    private static final String LOG_TAG = " ";

    private char[] password = "key-store-password".toCharArray();

    private static String hash;
    private static JSONObject carBrandJSON;
    private static ArrayList brandList;
    private static String vehicleNumber;
    private static String vehicleBrandModel;
    private static String vehicleBrand;
    private static int vehicleBrandID;
    private static int vehicleColorID;
    private static String vehicleModel;
    private static int vehicleModelID;
    private static int vehicleSetFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_set);

        brandList = new ArrayList<String>();
        final SparseArray brandSparse = new SparseArray();
        final ListView brandListView = (ListView) findViewById(R.id.brand_list_view);

        Intent intent = getIntent();
        vehicleNumber = intent.getStringExtra("VEHICLE_NUMBER_SAVE");
        vehicleBrandModel = intent.getStringExtra("VEHICLE_BRAND_MODEL_SAVE");
        vehicleColorID = intent.getIntExtra("VEHICLE_COLOR_ID_SAVE", 0);
        vehicleBrand = intent.getStringExtra("VEHICLE_BRAND_SAVE");
        vehicleBrandID = intent.getIntExtra("VEHICLE_BRAND_ID_SAVE", 0);
        vehicleModel = intent.getStringExtra("VEHICLE_MODEL_SAVE");
        vehicleModelID = intent.getIntExtra("VEHICLE_MODEL_ID_SAVE", 0);

        System.out.println("PRINT2: " + vehicleNumber + "      " + vehicleBrandModel + "      " + vehicleColorID +  "    " + vehicleBrandID + "    " +  vehicleModelID);

        try {
            String filesDirectory = getFilesDir().getAbsolutePath();
            String encryptedDataFilePath = filesDirectory + File.separator + "rocketstore.keystore";
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(new FileInputStream(encryptedDataFilePath), password);
            KeyStore.ProtectionParameter protParam =
                    new KeyStore.PasswordProtection(password);
            KeyStore.Entry entry = ks.getEntry("secretKeyAlias", protParam);
            SecretKey keyFound = ((KeyStore.SecretKeyEntry) entry).getSecretKey();
            hash = new String(keyFound.getEncoded());

        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        }

        try {
            carBrandJSON = new JSONObject(intent.getStringExtra("BRANDS_JSON"));
            JSONArray brandsArray = carBrandJSON.getJSONArray("result");

            for (int i = 0; i < brandsArray.length(); i++) {
                JSONObject brandObj = brandsArray.getJSONObject(i);
                brandSparse.put(brandObj.getInt("ID"), brandObj.optString("BRAND"));
                brandList.add(brandObj.optString("BRAND"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, brandList);

        brandListView.setAdapter(adapter);
        findViewById(R.id.loading_panel).setVisibility(View.GONE);

        brandListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String selectedItem = brandListView.getItemAtPosition(position).toString();
                //Log.d(LOG_TAG, "itemClick: position = " + position + ", id = "+ id + ", item = " + selectedItem);

                int indexValue = brandSparse.indexOfValue(selectedItem);
                final int keyValue = brandSparse.keyAt(indexValue);
                vehicleBrand = selectedItem;

                ResponseServer.getJSONObj("carBrands/" + keyValue, hash, new OnJSONResponseCallback() {
                    @Override
                    public void onJSONResponse(boolean success, JSONObject response) throws JSONException {
                        if (success) {
                            Intent intent = new Intent(BrandSetActivity.this, ModelSetActivity.class);
                            intent.putExtra("MODELS_JSON", response.toString());
                            intent.putExtra("VEHICLE_NUMBER_SAVE", vehicleNumber);
                            intent.putExtra("VEHICLE_BRAND_MODEL_SAVE", vehicleBrandModel);
                            intent.putExtra("VEHICLE_COLOR_ID_SAVE", vehicleColorID);
                            intent.putExtra("VEHICLE_BRAND_SAVE", vehicleBrand);
                            intent.putExtra("VEHICLE_BRAND_ID_SAVE", keyValue);
                            intent.putExtra("VEHICLE_MODEL_SAVE", vehicleModel);
                            intent.putExtra("VEHICLE_MODEL_ID_SAVE", vehicleModelID);
                            finish();
                            startActivity(intent);
                        } else {
                            AlertDialog.Builder builder =
                                    new AlertDialog.Builder(BrandSetActivity.this);
                            builder.setTitle("Ошибка!");
                            builder.setMessage("Не удается установить связь, попробуйте еще раз");

                            builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(BrandSetActivity.this, BrandSetActivity.class);
                                    dialog.dismiss();
                                    finish();
                                    startActivity(intent);
                                }
                            });
                            builder.show();
                        }
                    }
                });
            }
        });

        //System.out.println(brandSparse);
    }

    public void onClickClose(View v) {
        vehicleSetFlag = 1;
        Intent intent = new Intent(BrandSetActivity.this, AboutAutoActivity.class);
        intent.putExtra("VEHICLE_NUMBER_SAVE", vehicleNumber);
        intent.putExtra("VEHICLE_BRAND_MODEL_SAVE", vehicleBrandModel);
        intent.putExtra("VEHICLE_SET_FLAG_SAVE", vehicleSetFlag);
        intent.putExtra("VEHICLE_BRAND_SAVE", vehicleBrand);
        intent.putExtra("VEHICLE_MODEL_SAVE", vehicleModel);
        intent.putExtra("VEHICLE_MODEL_ID_SAVE", vehicleModelID);
        intent.putExtra("VEHICLE_COLOR_ID_SAVE", vehicleColorID);
        intent.putExtra("VEHICLE_BRAND_ID_SAVE", vehicleBrandID);
        System.out.println("PRINT3: " + vehicleNumber + "      " + vehicleBrandModel + "      " + vehicleColorID +  "    " + vehicleBrandID + "    " +  vehicleModelID);
        finish();
        startActivity(intent);
    }
}
