package com.rocket.driver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CountrySetActivity extends Activity {

    private static final String LOG_TAG = " ";
    private static JSONObject countryJSON;
    private static ArrayList countryList;
    private static String driverFirstName;
    private static String driverLastName;
    private static String driverEmail;
    private static String driverRegion;
    private static Boolean driverSetFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_set);

        countryList = new ArrayList<String>();
        final ListView countryListView = (ListView) findViewById(R.id.country_list_view);

        Intent intent = getIntent();
        driverFirstName = intent.getStringExtra("DRIVER_FIRST_NAME_SAVE");
        driverLastName = intent.getStringExtra("DRIVER_LAST_NAME_SAVE");
        driverEmail = intent.getStringExtra("DRIVER_EMAIL_SAVE");
        driverRegion = intent.getStringExtra("DRIVER_REGION_SAVE");

        try {
            countryJSON = new JSONObject(intent.getStringExtra("COUNTRY_JSON"));
            JSONArray countriesArray = countryJSON.getJSONArray("result");
            for (int i = 0; i < countriesArray.length(); i++) {
                JSONObject countryObj = countriesArray.getJSONObject(i);
                countryList.add(countryObj.names().toString().replaceAll("\"", "").replaceAll("\\[", "").replaceAll("\\]",""));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, countryList);

        countryListView.setAdapter(adapter);
        findViewById(R.id.loading_panel).setVisibility(View.GONE);

        countryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String selectedItem = countryListView.getItemAtPosition(position).toString();

                Intent intent = new Intent(CountrySetActivity.this, RegionSetActivity.class);
                intent.putExtra("COUNTRY_JSON", countryJSON.toString());
                intent.putExtra("SELECTED_COUNTRY", selectedItem);
                intent.putExtra("SELECTED_COUNTRY_ID", position);
                intent.putExtra("DRIVER_FIRST_NAME_SAVE", driverFirstName);
                intent.putExtra("DRIVER_LAST_NAME_SAVE", driverLastName);
                intent.putExtra("DRIVER_EMAIL_SAVE", driverEmail);
                finish();
                startActivity(intent);
            }
        });

        System.out.println(countryList);
    }

    public void onClickClose(View v) {
        driverSetFlag = true;
        Intent intent = new Intent(CountrySetActivity.this, AboutMeActivity.class);
        intent.putExtra("DRIVER_FIRST_NAME_SAVE", driverFirstName);
        intent.putExtra("DRIVER_LAST_NAME_SAVE", driverLastName);
        intent.putExtra("DRIVER_EMAIL_SAVE", driverEmail);
        intent.putExtra("DRIVER_REGION_SAVE", driverRegion);
        intent.putExtra("DRIVER_SET_FLAG_SAVE", driverSetFlag);
        finish();
        startActivity(intent);
    }
}
