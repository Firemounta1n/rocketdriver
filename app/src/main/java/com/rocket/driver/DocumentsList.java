package com.rocket.driver;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.ArrayList;

public class DocumentsList extends ArrayAdapter<String> {

    private final Activity context;
    private final ArrayList docName;
    private final ArrayList docImage;
    public DocumentsList(Activity context,
                         ArrayList docName, ArrayList docImage) {
        super(context, R.layout.list_single_document, docName);
        this.context = context;
        this.docName = docName;
        this.docImage = docImage;

    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.list_single_document, null, true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.text_document);

        txtTitle.setText(docName.get(position).toString());

        ImageView imageView = (ImageView) rowView.findViewById(R.id.image_document);

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .displayer(new RoundedBitmapDisplayer(30)) //rounded corner bitmap
                .cacheInMemory(true)
                .build();

        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        }

        imageLoader.displayImage(docImage.get(position).toString(), imageView, options);

        return rowView;
    }
}
