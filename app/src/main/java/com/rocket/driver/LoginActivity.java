package com.rocket.driver;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.PhoneNumberUtils;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import cz.msebera.android.httpclient.Header;

import com.mukesh.countrypicker.fragments.CountryPicker;
import com.mukesh.countrypicker.interfaces.CountryPickerListener;
import com.loopj.android.http.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class LoginActivity extends FragmentActivity  {

    private char[] password = "key-store-password".toCharArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        try {
            String filesDirectory = getFilesDir().getAbsolutePath();
            String encryptedDataFilePath = filesDirectory + File.separator + "rocketstore.keystore";
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(new FileInputStream(encryptedDataFilePath), password);

            if (!ks.isKeyEntry("secretKeyAlias")) {
                System.out.println("DOES NOT KEY");
            } else {
                Intent intent = new Intent(LoginActivity.this, VerificationActivity.class);
                startActivity(intent);
                finish();
            }

        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setContentView(R.layout.activity_login);

        EditText phoneNumber = (EditText) findViewById(R.id.edit_Number);
        phoneNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        SpannableString ss = new SpannableString("Нажимая «Войти», вы принимаете Условия пользования и Политику конфиденциальности.");
        ClickableSpan firstWordClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Uri uri = Uri.parse("http://rockettaxi.org/term/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        ClickableSpan secondWordClick = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Uri uri = Uri.parse("http://rockettaxi.org/privacydrivers/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(firstWordClick, 31, 50, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(secondWordClick, 53, 80, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        TextView textView = (TextView) findViewById(R.id.confident_Text);
        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.TRANSPARENT);
    }

    public void onClickCountry(View v) {
        final CountryPicker picker = CountryPicker.newInstance("Select Country");
        picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                TextView country = (TextView) findViewById(R.id.name_Country);
                TextView codeCountry = (TextView) findViewById(R.id.dial_Code_Country);
                country.setText(name);
                codeCountry.setText(dialCode);
                picker.onStop();
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void onClickEnter(View v) {
        TextView codeCountry = (TextView) findViewById(R.id.dial_Code_Country);
        EditText phoneNumber = (EditText) findViewById(R.id.edit_Number);

        String codePhone = codeCountry.getText().toString() + phoneNumber.getText().toString();
        final String resultPhone = PhoneNumberUtils.normalizeNumber(codePhone);

        if (resultPhone.length() != 12) {
            AlertDialog.Builder builder =
                    new AlertDialog.Builder(this);
            builder.setTitle("Внимание!");
            builder.setMessage("Укажите номер телефона");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.show();

        } else {

            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams("PHONE", resultPhone);

            client.post("http://api.rockettaxi.org:8888/api/0.1/login", params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    Intent intent = new Intent(LoginActivity.this, ValidationActivity.class);
                    intent.putExtra("DRIVER_PHONE_SAVE", resultPhone);
                    String response = new String(responseBody, StandardCharsets.UTF_8);
                    System.out.println("RESPONSE:   " + response);
                    finish();
                    startActivity(intent);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    AlertDialog.Builder builder =
                            new AlertDialog.Builder(LoginActivity.this);
                    builder.setTitle("Ошибка!");
                    builder.setMessage("Не удается установить связь, попробуйте еще раз");

                    builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
                            dialog.dismiss();
                            finish();
                            startActivity(intent);
                        }
                    });
                    builder.show();
                }
            });
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }

}
