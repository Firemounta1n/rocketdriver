package com.rocket.driver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ModelSetActivity extends Activity {

    private static final String LOG_TAG = " ";

    private static JSONObject carModelJSON;
    private static ArrayList modelList;
    private static String vehicleNumber;
    private static String vehicleBrandModel;
    private static String vehicleBrand;
    private static int vehicleBrandID;
    private static String vehicleModel;
    private static int vehicleModelID;
    private static int vehicleColorID;
    private static int vehicleSetFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_model_set);

        modelList = new ArrayList<String>();
        final SparseArray modelSparse = new SparseArray();
        final ListView modelListView = (ListView) findViewById(R.id.model_list_view);

        Intent intent = getIntent();
        vehicleNumber = intent.getStringExtra("VEHICLE_NUMBER_SAVE");
        vehicleBrandModel = intent.getStringExtra("VEHICLE_BRAND_MODEL_SAVE");
        vehicleColorID = intent.getIntExtra("VEHICLE_COLOR_ID_SAVE", 0);
        vehicleBrand = intent.getStringExtra("VEHICLE_BRAND_SAVE");
        vehicleBrandID = intent.getIntExtra("VEHICLE_BRAND_ID_SAVE", 0);
        vehicleModel = intent.getStringExtra("VEHICLE_MODEL_SAVE");
        vehicleModelID = intent.getIntExtra("VEHICLE_MODEL_ID_SAVE", 0);

        try {
            carModelJSON = new JSONObject(intent.getStringExtra("MODELS_JSON"));
            JSONArray modelsArray = carModelJSON.getJSONArray("result");

            for (int i = 0; i < modelsArray.length(); i++) {
                JSONObject modelObj = modelsArray.getJSONObject(i);
                modelSparse.put(modelObj.getInt("ID"), modelObj.optString("MODEL"));
                modelList.add(modelObj.optString("MODEL"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, modelList);

        modelListView.setAdapter(adapter);
        findViewById(R.id.loading_panel).setVisibility(View.GONE);

        modelListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String selectedItem = modelListView.getItemAtPosition(position).toString();
                //Log.d(LOG_TAG, "itemClick: position = " + position + ", id = "+ id + ", item = " + selectedItem);

                int indexValue = modelSparse.indexOfValue(selectedItem);
                final int keyValue = modelSparse.keyAt(indexValue);
                vehicleModel = selectedItem;
                vehicleSetFlag = 2;

                Intent intent = new Intent(ModelSetActivity.this, AboutAutoActivity.class);
                intent.putExtra("VEHICLE_NUMBER_SAVE", vehicleNumber);
                intent.putExtra("VEHICLE_BRAND_MODEL_SAVE", vehicleBrandModel);
                intent.putExtra("VEHICLE_COLOR_ID_SAVE", vehicleColorID);
                intent.putExtra("VEHICLE_BRAND_SAVE", vehicleBrand);
                intent.putExtra("VEHICLE_BRAND_ID_SAVE", vehicleBrandID);
                intent.putExtra("VEHICLE_MODEL_SAVE", vehicleModel);
                intent.putExtra("VEHICLE_MODEL_ID_SAVE", keyValue);
                intent.putExtra("VEHICLE_SET_FLAG_SAVE", vehicleSetFlag);
                finish();
                startActivity(intent);
            }
        });

        //System.out.println(modelSparse);

    }

    public void onClickBack(View v) {
        finish();
    }
}
