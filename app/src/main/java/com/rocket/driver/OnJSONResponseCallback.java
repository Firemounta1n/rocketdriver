package com.rocket.driver;

import org.json.JSONException;
import org.json.JSONObject;

public interface OnJSONResponseCallback {
     void onJSONResponse(boolean success, JSONObject response) throws JSONException;
}
