package com.rocket.driver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RegionSetActivity extends Activity {

    private static final String LOG_TAG = " ";
    private static JSONObject countryJSON;
    private static String selectedCountry;
    private static int selectedCountryID;
    private static ArrayList regionList;
    private static String driverFirstName;
    private static String driverLastName;
    private static String driverEmail;
    private static String driverRegion;
    private static Boolean driverSetFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_region_set);

        regionList = new ArrayList<String>();
        final SparseArray regionSparse = new SparseArray();
        final ListView regionListView = (ListView) findViewById(R.id.region_list_view);

        Intent intent = getIntent();
        selectedCountry = intent.getStringExtra("SELECTED_COUNTRY");
        selectedCountryID = intent.getIntExtra("SELECTED_COUNTRY_ID", 0);
        driverFirstName = intent.getStringExtra("DRIVER_FIRST_NAME_SAVE");
        driverLastName = intent.getStringExtra("DRIVER_LAST_NAME_SAVE");
        driverEmail = intent.getStringExtra("DRIVER_EMAIL_SAVE");

        try {
            countryJSON = new JSONObject(getIntent().getStringExtra("COUNTRY_JSON"));
            JSONArray countriesArray = countryJSON.getJSONArray("result");
            JSONArray regionsArray = countriesArray.getJSONObject(selectedCountryID).getJSONArray(selectedCountry);

            for (int i = 0; i < regionsArray.length(); i++) {
                JSONObject regionObj = regionsArray.getJSONObject(i);
                regionSparse.put(regionObj.getInt("ID"), regionObj.optString("NAME"));
                regionList.add(regionObj.optString("NAME"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, regionList);

        regionListView.setAdapter(adapter);
        findViewById(R.id.loading_panel).setVisibility(View.GONE);

        regionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String selectedItem = regionListView.getItemAtPosition(position).toString();

                int indexValue = regionSparse.indexOfValue(selectedItem);
                int keyValue = regionSparse.keyAt(indexValue);
                driverRegion = selectedItem;
                driverSetFlag = true;

                Intent intent = new Intent(RegionSetActivity.this, AboutMeActivity.class);
                intent.putExtra("DRIVER_FIRST_NAME_SAVE", driverFirstName);
                intent.putExtra("DRIVER_LAST_NAME_SAVE", driverLastName);
                intent.putExtra("DRIVER_EMAIL_SAVE", driverEmail);
                intent.putExtra("DRIVER_REGION_SAVE", driverRegion);
                intent.putExtra("SELECTED_COUNTRY_ID", keyValue);
                intent.putExtra("DRIVER_SET_FLAG_SAVE", driverSetFlag);
                finish();
                startActivity(intent);
            }
        });

        System.out.println(regionSparse);

    }

    public void onClickBack(View v) {
        finish();
    }
}
