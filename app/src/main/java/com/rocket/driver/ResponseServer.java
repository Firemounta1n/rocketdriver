package com.rocket.driver;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class ResponseServer {

    public static void postJSONObj(String address, String hash, final OnJSONResponseCallback callback) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.post("http://api.rockettaxi.org:8888/api/0.1/"+ address +"?hash=" + hash, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, JSONObject response) {
                try {
                    callback.onJSONResponse(true, response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject errorResponse) {
                try {
                    callback.onJSONResponse(false, errorResponse);
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                Log.e("Exception", "JSONException " + e.toString());
            }
        });
    }

    public static void getJSONObj(String address, String hash, final OnJSONResponseCallback callback) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://api.rockettaxi.org:8888/api/0.1/"+ address +"?hash=" + hash, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, JSONObject response) {
                try {
                    callback.onJSONResponse(true, response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject errorResponse) {
                try {
                    callback.onJSONResponse(false, errorResponse);
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                Log.e("Exception", "JSONException " + e.toString());
            }
        });
    }
}
