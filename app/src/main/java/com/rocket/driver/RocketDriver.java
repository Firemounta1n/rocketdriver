package com.rocket.driver;

import android.app.Application;
import io.smooch.core.Settings;
import io.smooch.core.Smooch;

public class RocketDriver extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Settings settings = new Settings("7w4jzvn83vy0ihq26jqxxpblm");
        settings.setFileProviderAuthorities("com.example.android.fileprovider");
        Smooch.init(this, settings);
    }
}
