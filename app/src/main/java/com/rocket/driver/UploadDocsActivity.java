package com.rocket.driver;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.crypto.SecretKey;

import cz.msebera.android.httpclient.Header;

public class UploadDocsActivity extends Activity{

    private static final int PERMISSION_REQUEST_CODE = 2;
    private char[] password = "key-store-password".toCharArray();

    private static String hash;
    private static JSONObject documentsJSON;
    private static JSONObject imagesJSON;
    private static ArrayList docsArrayList = new ArrayList<String>();
    private static ArrayList imagesArrayList = new ArrayList<String>();
    private static int imagePosition;
    private static String mCurrentPhotoPath;
    private static ListView docsListView;
    private static File imageFile;
    private static final int REQUEST_TAKE_PHOTO = 1;
    private static final int READ_REQUEST_CODE = 42;
    private static SparseArray docsSparse = new SparseArray();
    private static SparseArray imagesSparse = new SparseArray();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_docs);

        try {
            String filesDirectory = getFilesDir().getAbsolutePath();
            String encryptedDataFilePath = filesDirectory + File.separator + "rocketstore.keystore";
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(new FileInputStream(encryptedDataFilePath), password);
            KeyStore.ProtectionParameter protParam =
                    new KeyStore.PasswordProtection(password);
            KeyStore.Entry entry = ks.getEntry("secretKeyAlias", protParam);
            SecretKey keyFound = ((KeyStore.SecretKeyEntry) entry).getSecretKey();
            hash = new String(keyFound.getEncoded());
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        }

        ResponseServer.getJSONObj("countryDocuments", hash, new OnJSONResponseCallback(){
            @Override
            public void onJSONResponse(boolean success, JSONObject response) throws JSONException {
                if (success) {
                    documentsJSON = response;
                    System.out.println("Docs from server:   " + documentsJSON.toString());

                    ResponseServer.getJSONObj("documents", hash, new OnJSONResponseCallback(){
                        @Override
                        public void onJSONResponse(boolean success, JSONObject response) throws JSONException {
                            if (success) {
                                imagesJSON = response;

                                System.out.println("Images from server:   " + imagesJSON.toString());

                                JSONArray imagesArray = imagesJSON.getJSONArray("result");
                                imagesArrayList = new ArrayList<String>();
                                imagesSparse = new SparseArray();

                                JSONArray docsArray = documentsJSON.getJSONArray("result");
                                docsArrayList = new ArrayList<String>();
                                docsSparse = new SparseArray();

                                for (int i = 0; i < docsArray.length(); i++) {
                                    JSONObject docsObj = docsArray.getJSONObject(i);
                                    docsSparse.put(docsObj.getInt("ID"), docsObj.optString("NAME"));
                                    docsArrayList.add(docsObj.optString("NAME"));
                                    imagesArrayList.add("drawable://" + R.drawable.image_default);
                                }

                                for (int i = 0; i < imagesArray.length(); i++) {
                                    JSONObject imagesObj = imagesArray.getJSONObject(i);
                                    imagesSparse.put(imagesObj.getInt("documentTypeId"), imagesObj.optString("url"));

                                }

                                System.out.println("imagesArrayLength: " + imagesArray.length());
                                System.out.println("imageArray: " + imagesArrayList);
                                System.out.println("imageSparse: " + imagesSparse);
                                System.out.println("docsArray: " + docsArrayList);
                                System.out.println("docsSparse: " + docsSparse);

                                    for (int i = 0; i < imagesArray.length(); i++) {
                                        try {
                                        imagesArrayList.remove(docsArrayList.indexOf(docsSparse.get(imagesSparse.keyAt(i))));
                                        imagesArrayList.add(docsArrayList.indexOf(docsSparse.get(imagesSparse.keyAt(i))), "http://api.rockettaxi.org:80" + imagesSparse.valueAt(i));
                                        } catch (ArrayIndexOutOfBoundsException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                final DocumentsList adapter = new
                                        DocumentsList(UploadDocsActivity.this, docsArrayList, imagesArrayList);

                                docsListView=(ListView)findViewById(R.id.list_single_document);

                                docsListView.setAdapter(adapter);

                                docsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view,
                                                            int position, long id) {

                                        //Toast.makeText(UploadDocsActivity.this, "You Clicked at " + imagesArrayList.get(+position), Toast.LENGTH_SHORT).show();
                                        docsListView.getChildAt(position);
                                        imagePosition = position;

                                        AlertDialog.Builder builder =
                                                new AlertDialog.Builder(UploadDocsActivity.this);
                                        builder.setTitle("Выберите изображение");

                                        builder.setPositiveButton("Галерея", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (ContextCompat.checkSelfPermission(UploadDocsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                                        == PackageManager.PERMISSION_GRANTED) {
                                                    performFileSearch();
                                                    dialog.dismiss();
                                                }

                                                if (ContextCompat.checkSelfPermission(UploadDocsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                                        == PackageManager.PERMISSION_DENIED) {
                                                    requestMultiplePermissions();
                                                    dialog.dismiss();
                                                }
                                            }
                                        });

                                        builder.setNegativeButton("Камера", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (ContextCompat.checkSelfPermission(UploadDocsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                                        == PackageManager.PERMISSION_GRANTED) {
                                                    dispatchTakePictureIntent();
                                                    dialog.dismiss();
                                                }

                                                if (ContextCompat.checkSelfPermission(UploadDocsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                                        == PackageManager.PERMISSION_DENIED) {
                                                    requestMultiplePermissions();
                                                    dialog.dismiss();
                                                }
                                            }
                                        });

                                        builder.show();
                                    }
                                });
                                findViewById(R.id.loading_panel).setVisibility(View.GONE);
                            } else {
                                AlertDialog.Builder builder =
                                        new AlertDialog.Builder(UploadDocsActivity.this);
                                builder.setTitle("Ошибка!");
                                builder.setMessage("Не удается установить связь, попробуйте еще раз");

                                builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(UploadDocsActivity.this, UploadDocsActivity.class);
                                        dialog.dismiss();
                                        finish();
                                        startActivity(intent);
                                    }
                                });
                                builder.show();

                            }
                        }
                    });
                } else {
                    AlertDialog.Builder builder =
                            new AlertDialog.Builder(UploadDocsActivity.this);
                    builder.setTitle("Ошибка!");
                    builder.setMessage("Не удается установить связь, попробуйте еще раз");

                    builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(UploadDocsActivity.this, UploadDocsActivity.class);
                            dialog.dismiss();
                            finish();
                            startActivity(intent);
                        }
                    });
                    builder.show();
                }
            }
        });
    }

    public void onClickReady(View v) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(UploadDocsActivity.this);
        builder.setTitle("Внимание!");
        builder.setMessage("Вы уверены, что выбранные Вами документы верны?");

        builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(UploadDocsActivity.this, VerificationActivity.class);
                finish();
                startActivity(intent);
            }
        });

        builder.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();
    }

    public void onClickClose(View v) {
        Intent intent = new Intent(UploadDocsActivity.this, VerificationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        finish();
        startActivity(intent);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file://" + image.getAbsolutePath();
        imageFile = image;
        return image;
    }

    private void performFileSearch() {

        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
        // browser.
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // Filter to show only images, using the image MIME data type.
        // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
        // To search for all documents available via installed storage providers,
        // it would be "*/*".
        intent.setType("image/*");

        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    private File saveBitmap(Bitmap bmp) throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        final File externalStorage = Environment.getExternalStorageDirectory();

        final File f = new File(externalStorage
                + File.separator + "testimage.jpg");
        f.createNewFile();
        FileOutputStream fo = new FileOutputStream(f);
        fo.write(bytes.toByteArray());
        fo.close();
        mCurrentPhotoPath = "file://" + f.getAbsolutePath();
        return f;
    }

    public void requestMultiplePermissions() {
        ActivityCompat.requestPermissions(this,
                new String[] {
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                },
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE && grantResults.length == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                performFileSearch();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {

        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {

            imagesArrayList.remove(imagePosition);
            imagesArrayList.add(imagePosition, mCurrentPhotoPath);

            String docString = docsArrayList.get(imagePosition).toString();

            int docID = docsSparse.keyAt(docsSparse.indexOfValue(docString));

            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();

            try {
                params.put("documentType", docID);
                params.put("doc", imageFile);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            client.post("http://api.rockettaxi.org:8888/api/0.1/documents?hash=" + hash, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                //Intent intent = new Intent(UploadDocsActivity.this, VerificationActivity.class);
                //startActivity(intent);
                System.out.println("STATUS CODE:  " + statusCode);
                try {
                    imageFile.delete();
                    String str = new String(responseBody, "UTF-8");
                    System.out.println("ResponseBody:  " + str);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String str = null;
                try {
                    imageFile.delete();
                    str = new String(responseBody, "UTF-8");
                    System.out.println("ResponseBody:  " + str + "Error:   " + error);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                AlertDialog.Builder builder =
                        new AlertDialog.Builder(UploadDocsActivity.this);
                builder.setTitle("Ошибка!");
                builder.setMessage("Не удается установить связь, попробуйте еще раз");

                builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(UploadDocsActivity.this, UploadDocsActivity.class);
                        dialog.dismiss();
                        finish();
                        startActivity(intent);
                    }
                });
                builder.show();

            }

            });

        }

        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = null;
            if (resultData != null) {
                uri = resultData.getData();

                try {
                    Bitmap bitmapImage = getBitmapFromUri(uri);
                    File newImageFile = saveBitmap(bitmapImage);
                    imageFile = newImageFile;
                } catch (IOException e) {
                    e.printStackTrace();
                }

                View v = docsListView.getChildAt(imagePosition -
                        docsListView.getFirstVisiblePosition());

                if(v == null)
                    return;

                mCurrentPhotoPath = uri.toString();

                imagesArrayList.remove(imagePosition);
                imagesArrayList.add(imagePosition, mCurrentPhotoPath);


                ImageView someImage = (ImageView) v.findViewById(R.id.image_document);

                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(UploadDocsActivity.this)
                        .discCacheFileNameGenerator(new HashCodeFileNameGenerator())
                        .build();

                DisplayImageOptions options = new DisplayImageOptions.Builder()
                        .displayer(new RoundedBitmapDisplayer(30))
                        .cacheInMemory(true)
                        .cacheOnDisc(true)
                        .build();

                ImageLoader imageLoader = ImageLoader.getInstance();

                if (!imageLoader.isInited()) {
                    imageLoader.init(config);
                }
                imageLoader.displayImage(uri.toString(), someImage, options);

                String docString = docsArrayList.get(imagePosition).toString();

                int docID = docsSparse.keyAt(docsSparse.indexOfValue(docString));

                AsyncHttpClient client = new AsyncHttpClient();
                RequestParams params = new RequestParams();

                try {
                    params.put("documentType", docID);
                    params.put("doc", imageFile);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                client.post("http://api.rockettaxi.org:8888/api/0.1/documents?hash=" + hash, params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        System.out.println("STATUS CODE:  " + statusCode);
                        try {
                            imageFile.delete();
                            String str = new String(responseBody, "UTF-8");
                            System.out.println("ResponseBody:  " + str);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        String str = null;
                        try {
                            imageFile.delete();
                            str = new String(responseBody, "UTF-8");
                            System.out.println("ResponseBody:  " + str + "Error:   " + error);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                        AlertDialog.Builder builder =
                                new AlertDialog.Builder(UploadDocsActivity.this);
                        builder.setTitle("Ошибка!");
                        builder.setMessage("Не удается установить связь, попробуйте еще раз");

                        builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(UploadDocsActivity.this, UploadDocsActivity.class);
                                dialog.dismiss();
                                finish();
                                startActivity(intent);
                            }
                        });
                        builder.show();

                    }
                });

            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }
}
