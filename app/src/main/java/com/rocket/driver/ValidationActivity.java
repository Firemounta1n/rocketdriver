package com.rocket.driver;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import io.smooch.core.User;

import org.json.*;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class ValidationActivity extends Activity {

    private static String phoneNumber;
    private static JSONObject driverInfoJSON;
    private static String hash;
    private static String verified;
    private static String driverID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validation);

        Intent intent = getIntent();
        phoneNumber = intent.getStringExtra("DRIVER_PHONE_SAVE");
    }

    public void onClickReady(View v) {
        EditText textValidationCode = (EditText) findViewById(R.id.validation_Code);
        String validationCode = textValidationCode.getText().toString();

        if (validationCode.length() != 4) {
            AlertDialog.Builder builder =
                    new AlertDialog.Builder(ValidationActivity.this);
            builder.setTitle("Ошибка");
            builder.setMessage("Укажите код");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builder.show();

        } else {

            final AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            System.out.println("PHONE_NUMBER:  " + phoneNumber + "     SMS:    " + validationCode);
            params.put("PHONE", phoneNumber);
            params.put("SMS", validationCode);

            client.post("http://api.rockettaxi.org:8888/api/0.1/login", params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        System.out.println("STATUS_CODE:    " + statusCode);
                        System.out.println("RESPONSE:    " + response);

                        driverInfoJSON = response.getJSONObject("result");
                        hash = driverInfoJSON.getString("hash");
                        verified = driverInfoJSON.getString("isVerified");
                        driverID = driverInfoJSON.getString("driverId");

                        System.out.println("VERIFIED:    " + verified);
                        System.out.println("HASH:    " + hash);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    final Map<String, Object> customProperties = new HashMap<>();
                    customProperties.put("driverID", driverID);
                    customProperties.put("driverPhone", phoneNumber);
                    User.getCurrentUser().addProperties(customProperties);

                    try {
                        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
                        ks.load(null);
                        byte[] sek = hash.getBytes();
                        SecretKey sk = new SecretKeySpec(sek, 0, sek.length, "AES");
                        char[] password = "key-store-password".toCharArray();
                        KeyStore.ProtectionParameter protParam =
                                new KeyStore.PasswordProtection(password);
                        KeyStore.SecretKeyEntry skEntry = new KeyStore.SecretKeyEntry(sk);
                        ks.setEntry("secretKeyAlias", skEntry, protParam);
                        String filesDirectory = getFilesDir().getAbsolutePath();
                        String encryptedDataFilePath = filesDirectory + File.separator + "rocketstore.keystore";
                        ks.store(new FileOutputStream(encryptedDataFilePath), password);
                    } catch (KeyStoreException e) {
                        e.printStackTrace();
                    } catch (CertificateException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (verified == "true") {
                        Intent intent = new Intent(ValidationActivity.this, MainActivity.class);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(ValidationActivity.this, VerificationActivity.class);
                        startActivity(intent);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject errorResponse) {
                    AlertDialog.Builder builder =
                            new AlertDialog.Builder(ValidationActivity.this);
                    builder.setTitle("Ошибка!");
                    builder.setMessage("Не удается установить связь, попробуйте еще раз");

                    builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(ValidationActivity.this, LoginActivity.class);
                            dialog.dismiss();
                            finish();
                            startActivity(intent);
                        }
                    });
                    builder.show();
                }
            });
        }
    }

    public void onClickBack(View v) {
        Intent intent = new Intent(ValidationActivity.this, LoginActivity.class);
        finish();
        startActivity(intent);
    }

    public void onClickRepeat(View v) {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams("PHONE", phoneNumber);

        client.post("http://api.rockettaxi.org:8888/api/0.1/login", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(ValidationActivity.this);
                builder.setTitle("Внимание");
                builder.setMessage("Код отправлен повторно, если Вы не получили SMS сообщение с кодом, проверьте правильность введенного Вами телефона");

                builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(ValidationActivity.this);
                builder.setTitle("Ошибка!");
                builder.setMessage("Не удается установить связь, попробуйте еще раз");

                builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }
}
