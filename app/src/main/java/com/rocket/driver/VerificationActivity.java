package com.rocket.driver;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

import javax.crypto.SecretKey;

import cz.msebera.android.httpclient.Header;
import io.smooch.ui.ConversationActivity;

public class VerificationActivity extends Activity{

    private static String hash;
    private static Boolean driverVerified;
    private static int driverStep;

    private char[] password = "key-store-password".toCharArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        try {
            String filesDirectory = getFilesDir().getAbsolutePath();
            String encryptedDataFilePath = filesDirectory + File.separator + "rocketstore.keystore";
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(new FileInputStream(encryptedDataFilePath), password);

            KeyStore.ProtectionParameter protParam =
                    new KeyStore.PasswordProtection(password);
            KeyStore.Entry entry = ks.getEntry("secretKeyAlias", protParam);

            SecretKey keyFound = ((KeyStore.SecretKeyEntry) entry).getSecretKey();
            hash = new String(keyFound.getEncoded());

        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://api.rockettaxi.org:8888/api/0.1/verified?hash=" + hash, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                ImageView imageAboutMe = (ImageView) findViewById(R.id.image_about_me);
                ImageView imageAboutAuto = (ImageView) findViewById(R.id.image_about_auto);
                ImageView imageUploadDocs = (ImageView) findViewById(R.id.image_upload_docs);

                FrameLayout frameAboutMe = (FrameLayout) findViewById(R.id.frame_about_me);
                FrameLayout frameAboutAuto = (FrameLayout) findViewById(R.id.frame_about_auto);
                FrameLayout frameUploadDocs = (FrameLayout) findViewById(R.id.frame_upload_docs);

                frameAboutAuto.setClickable(false);
                frameUploadDocs.setClickable(false);

                TextView textWait = (TextView) findViewById(R.id.text_wait);

                System.out.println(response.toString());

                driverStep = 0;

                try {
                    JSONObject driverInfo = response.getJSONObject("result");
                    driverVerified = driverInfo.getBoolean("isVerified");
                    driverStep = driverInfo.getInt("step");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (driverStep == 0 && !driverVerified) {
                    imageAboutMe.setImageResource(R.drawable.ic_complete_add);
                    imageAboutAuto.setImageResource(R.drawable.ic_complete_add);
                    imageUploadDocs.setImageResource(R.drawable.ic_complete_add);

                    imageAboutAuto.setVisibility(View.VISIBLE);
                    imageUploadDocs.setVisibility(View.VISIBLE);

                    frameAboutMe.setClickable(true);
                    frameAboutAuto.setClickable(true);
                    frameUploadDocs.setClickable(true);

                    textWait.setVisibility(View.VISIBLE);
                } else {

                    switch (driverStep) {
                        case 2:
                            imageAboutMe.setImageResource(R.drawable.ic_complete_add);

                            imageAboutAuto.setVisibility(View.VISIBLE);

                            frameAboutMe.setClickable(false);
                            frameAboutAuto.setClickable(true);
                            break;
                        case 3:
                            imageAboutMe.setImageResource(R.drawable.ic_complete_add);
                            imageAboutAuto.setImageResource(R.drawable.ic_complete_add);

                            imageAboutAuto.setVisibility(View.VISIBLE);
                            imageUploadDocs.setVisibility(View.VISIBLE);

                            frameAboutMe.setClickable(false);
                            frameUploadDocs.setClickable(true);
                            break;
                    }
                }
                findViewById(R.id.loading_panel).setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject errorResponse) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(VerificationActivity.this);
                builder.setTitle("Ошибка!");
                builder.setMessage("Не удается установить связь, попробуйте еще раз");

                builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(VerificationActivity.this, VerificationActivity.class);
                        dialog.dismiss();
                        finish();
                        startActivity(intent);
                    }
                });
                builder.show();
            }
        });

    }

    public void onClickAboutMe(View v) {
        Intent intent = new Intent(VerificationActivity.this, AboutMeActivity.class);
        startActivity(intent);
    }

    public void onClickAboutAuto(View v) {
        Intent intent = new Intent(VerificationActivity.this, AboutAutoActivity.class);
        startActivity(intent);
    }

    public void onClickUploadDocs(View v) {
        Intent intent = new Intent(VerificationActivity.this, UploadDocsActivity.class);
        startActivity(intent);
    }

    public void onClickExit(View v) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Внимание!");
        builder.setMessage("Вы уверены, что хотите выйти?");

        builder.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    String filesDirectory = getFilesDir().getAbsolutePath();
                    String encryptedDataFilePath = filesDirectory + File.separator + "rocketstore.keystore";

                    KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
                    ks.load(new FileInputStream(encryptedDataFilePath), password);
                    ks.deleteEntry("secretKeyAlias");
                    ks.store(new FileOutputStream(encryptedDataFilePath), password);

                    Intent intent = new Intent(VerificationActivity.this, LoginActivity.class);
                    finish();
                    startActivity(intent);
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                } catch (CertificateException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void onClickSupport(View v) {
        ConversationActivity.show(this);
    }
}